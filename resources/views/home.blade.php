@extends('layouts.app')

@section('content')
				<div class="container">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Users
                    </div>
                    <div class="panel-body"><!-- use bootstrap sortable for table sorting-->
                        <table class="table table-bordered table-striped sortable">
                            <thead>
                                <th>name</th>
                                <th>email</th>
                                <th>realName</th>
                                <th>surname</th>
                                <th>age</th>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>
                                            {{ $user->name }}
                                        </td>
                                        <td>{{ $user->email }} </td>
										<td> {{ $user->realName }}</td>
                                        <td> {{ $user->surname }}</td>
                                        <td> {{ $user->age }} </td>
                                   </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
				</div>
@endsection
